"""
Settings module for the forwarding service
"""

from decouple import config

WEBDAV_URL = config('WEBDAV_URL', default='')
SENSOR_KEY = config('SENSOR_KEY', default='')
LIGHTCURVE_WEBDAV_USERNAME = config('LIGHTCURVE_WEBDAV_USERNAME', default='')
LIGHTCURVE_WEBDAV_KEY = config('LIGHTCURVE_WEBDAV_KEY', default='')
TDMCONVERT_WEBDAV_USERNAME = config('TDMCONVERT_WEBDAV_USERNAME', default='')
TDMCONVERT_WEBDAV_KEY = config('TDMCONVERT_WEBDAV_KEY', default='')
