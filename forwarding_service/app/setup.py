"""
This module provides the Forwarding Service Flask WSGI App.
"""
from flask import Flask


def create_app():
    """
    Setup the Flask Application

    This function can be called by a wsgi server like gunicorn
    to load this application.
    """
    app = Flask(__name__)

    # pylint: disable=import-outside-toplevel, no-name-in-module
    from . import api_views
    app.register_blueprint(api_views.BP)

    return app
