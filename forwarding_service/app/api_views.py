"""
This module provides the Forwarding Service API views with the following HTTP REST API endpoint:
- POST `/forward_tdms`
"""

import base64
# - Pylint doesn't dsicover app.webdav_utils (but it works when deployed)
# pylint: pylint: disable=import-error
import json
import os
import re
import shutil
import tempfile
import zipfile
from datetime import datetime
from http import HTTPStatus

import requests
from eon_opt02 import LightCurve
from flask import Blueprint, Response, jsonify, render_template, request, send_file
from prometheus_client import CONTENT_TYPE_LATEST, Counter, generate_latest
from werkzeug.utils import secure_filename

import app.settings as settings

BP = Blueprint("api", __name__, url_prefix="")

ERROR_COUNTER = Counter("error_count", "Number of errors", ["endpoint", "error_type"])


def analyse_cleanup(file_path):
    """Removes temporary files"""
    os.remove(file_path)
    print(f"Removed file: {file_path}", flush=True)


def create_directory(url, headers):
    """Creates a directory through WEBDAV"""
    response = requests.request("MKCOL", url, headers=headers)
    if response.status_code not in [201, 405]:
        response.raise_for_status()


def upload_lightcurve_to_datacube(file_timestamp, filepath):
    """Uploads a file to Lightcurve Datacube, creating necessary directories"""
    def lightcurve_upload_file_to_webdav(base_url, headers, filename, filepath):
        full_url = f"{base_url}/{filename}"
        with open(filepath, "rb") as the_file:
            file_data = the_file.read()
        response = requests.put(full_url, headers=headers, data=file_data)
        response.raise_for_status()

    base_webdav_url = settings.WEBDAV_URL
    year = file_timestamp[:4]
    month = file_timestamp[4:6]
    day = file_timestamp[6:8]

    username = settings.LIGHTCURVE_WEBDAV_USERNAME
    apikey = settings.LIGHTCURVE_WEBDAV_KEY
    filename = f"output_{file_timestamp}.zip"

    encoded_string = f"{username}:{apikey}".encode("utf-8")
    token = base64.b64encode(encoded_string).decode("utf-8")
    headers = {"Authorization": f"Basic {token}", "Content-type": "application/zip"}

    create_directory(f"{base_webdav_url}/{year}", headers)
    create_directory(f"{base_webdav_url}/{year}/{month}", headers)
    create_directory(f"{base_webdav_url}/{year}/{month}/{day}", headers)

    lightcurve_upload_file_to_webdav(f"{base_webdav_url}/{year}/{month}/{day}", headers, filename,
                                     filepath)


def upload_tdm_to_datacube(file_timestamp, file, filename):
    """Uploads a file to TDMConvert Datacube, creating necessary directories"""
    def tdm_upload_file_to_webdav(base_url, headers, filename, file_data):
        full_url = f"{base_url}/{filename}"
        response = requests.put(full_url, headers=headers, data=file_data)
        response.raise_for_status()

    base_webdav_url = settings.WEBDAV_URL
    year = file_timestamp[:4]
    month = file_timestamp[4:6]
    day = file_timestamp[6:8]

    username = settings.TDMCONVERT_WEBDAV_USERNAME
    apikey = settings.TDMCONVERT_WEBDAV_KEY

    encoded_string = f"{username}:{apikey}".encode("utf-8")
    token = base64.b64encode(encoded_string).decode("utf-8")
    headers = {"Authorization": f"Basic {token}", "Content-type": "application/zip"}

    create_directory(f"{base_webdav_url}/{year}", headers)
    create_directory(f"{base_webdav_url}/{year}/{month}", headers)
    create_directory(f"{base_webdav_url}/{year}/{month}/{day}", headers)

    tdm_upload_file_to_webdav(f"{base_webdav_url}/{year}/{month}/{day}", headers, filename, file)


@BP.route("/analyse-ui-multiple", methods=["GET", "POST"])
def analyse_ui_multiple():  # pylint: disable=R0914,R0914,R0912,R0915,R0911
    """Analyses multiple lightcurve input files and renders their respective plots"""
    if request.method == "POST":
        output_plot_data = {}
        with tempfile.TemporaryDirectory() as tempdir_name:
            # Iterate over uploaded files
            for file_key in request.files:
                input_kvn = request.files[file_key]

                if not (input_kvn.filename.endswith(".xml") or input_kvn.filename.endswith(".kvn")
                        or input_kvn.filename.endswith(".tdm")):
                    error_message = ("Invalid file type (expected .xml or .tdm or .kvn extension)")
                    ERROR_COUNTER.labels(endpoint=request.path,
                                         error_type="Wrong file extension").inc()
                    return (
                        render_template("lightcurve_error.html", message=error_message),
                        400,
                    )

                # If the norad is a valid integer, treat it as norad_id just for directory naming
                try:
                    norad, file_no = secure_filename(file_key).split("_")
                    subdir_name = os.path.join(tempdir_name, f"noradID_{int(norad)}")
                    file_no = int(file_no)
                except ValueError:
                    error_message = (
                        f"Invalid type for file key '{secure_filename(file_key)}'"
                        f"(must be in the format 'noradID_filenumber', e.g. '12345_1')")
                    ERROR_COUNTER.labels(
                        endpoint=request.path,
                        error_type="File field name not following convention",
                    ).inc()
                    return (
                        render_template("lightcurve_error.html", message=error_message),
                        400,
                    )

                os.makedirs(subdir_name, exist_ok=True)
                input_filename = os.path.join(tempdir_name, secure_filename(input_kvn.filename))
                input_kvn.save(input_filename)

                if not output_plot_data.get(f"{norad}"):
                    output_plot_data[f"{norad}"] = []

                try:
                    lightcurve = LightCurve(input_filename, acc_key=settings.SENSOR_KEY)
                except AttributeError:
                    error_message = "Error in the file contents. Is SENSORID missing?"
                    ERROR_COUNTER.labels(
                        endpoint=request.path,
                        error_type="Missing attribute in input file",
                    ).inc()
                    return (
                        render_template("lightcurve_error.html", message=error_message),
                        400,
                    )
                lightcurve.analyse(export=subdir_name)
                x_axis = lightcurve.DT.tolist()
                y_axis = lightcurve.MAG.tolist()
                sin_model = lightcurve.total_signal1.tolist()
                output_plot_data[f"{norad}"] = [x_axis, y_axis, sin_model]

                history_filename = os.path.join(subdir_name, f"history_{norad}.txt")
                total_history_filename = os.path.join(subdir_name, f"history_{norad}_total.txt")
                if not os.path.exists(total_history_filename):
                    try:
                        shutil.copy(history_filename, total_history_filename)
                    except FileNotFoundError:
                        error_message = (
                            "NoradID in file key does not correspond to NoradId in file contents"
                            "(must be in the format 'noradID_filenumber', e.g. '12345_1')")
                        ERROR_COUNTER.labels(
                            endpoint=request.path,
                            error_type="NoradID in file key does not correspond \
                            to NoradId in file contents",
                        ).inc()
                        return (
                            render_template("lightcurve_error.html", message=error_message),
                            400,
                        )
                else:
                    with open(history_filename, "r", encoding="utf-8") as source_file, open(
                            total_history_filename, "a", encoding="utf-8") as target_file:
                        last_line = source_file.readlines()[-1]
                        target_file.write(last_line)
                os.remove(history_filename)
                os.remove(input_filename)

            history_filename_pattern = re.compile(r"history_(\d+)_total\.txt")
            histories = {}
            for root, _, files in os.walk(tempdir_name):
                for file in files:
                    match = history_filename_pattern.match(file)
                    if match:
                        norad = int(match.group(1))
                        with open(os.path.join(root, file), "r", encoding="utf8") as history_file:
                            content = history_file.read()
                            histories[norad] = content

            fp_zip_file_path = None

            with tempfile.NamedTemporaryFile("w+b", delete=False) as fp_zip_file:
                output_filenames = os.listdir(tempdir_name)

                if len(output_filenames) == 0:
                    error_message = ("Lightcurve analysis module did not create any output files.")
                    ERROR_COUNTER.labels(endpoint=request.path,
                                         error_type="No output files created").inc()
                    return (
                        render_template("lightcurve_error.html", message=error_message),
                        400,
                    )

                with zipfile.ZipFile(fp_zip_file.name, "w", zipfile.ZIP_DEFLATED) as zip_file:
                    for root, _, files in os.walk(tempdir_name):
                        for file in files:
                            full_path = os.path.join(root, file)
                            relative_path = os.path.relpath(full_path, tempdir_name)
                            zip_file.write(full_path, arcname=relative_path)

                fp_zip_file.seek(0)
                zipfile_timestamp = datetime.now().strftime("%Y%m%d%H%M%S%f")
                fp_zip_file_path = fp_zip_file.name

            try:
                upload_lightcurve_to_datacube(zipfile_timestamp, fp_zip_file_path)
                analyse_cleanup(fp_zip_file_path)
            except requests.exceptions.RequestException:
                error_message = "Could not upload analysis results to DataCube"
                ERROR_COUNTER.labels(endpoint=request.path,
                                     error_type="Failure to upload results").inc()
                analyse_cleanup(fp_zip_file_path)
                return (
                    render_template("lightcurve_error.html", message=error_message),
                    500,
                )

        return render_template(
            "lightcurve_chart_multiple.html",
            plot_data=json.dumps(output_plot_data),
            norads=output_plot_data.keys(),
            histories=histories,
        )
    return render_template("lightcurve_upload_multiple.html")


@BP.route("/")
def hello_world():
    """
    This dummy endpoint returns just a dummy message and serves testing purposes only.
    """
    return jsonify(hello="world")


@BP.route("/forward_tdms", methods=["PUT"])
def forward_tdms():
    """
    This endpoint accepts a set of CCSDS TDM messages and forwards them via WebDAV.
    """
    data = json.loads(request.data)
    print(f"Received {len(data['tdms'])} TDM message(s), forwarding...")
    forward_timestamp = datetime.now().strftime("%Y%m%d%H%M%S%f")
    try:
        for filename, data in data["tdms"].items():
            upload_tdm_to_datacube(forward_timestamp, data, filename)
        return Response(json.dumps({"message": "Success"}), mimetype="application/json")
    except requests.exceptions.RequestException as err:
        print(f"Upload failed with {err}")
        return Response(
            json.dumps({
                "message": "Error",
                "details": str(err)
            }),
            mimetype="application/json",
            status=HTTPStatus.INTERNAL_SERVER_ERROR,
        )


EON_OPT02_ANALYSIS_PARAMS_NAMES = [
    "period_max",
    "period_min",
    "period_step",
    "fap_limit",
    "long_period_peak_ratio",
    "cleaning_max_power_ratio",
    "cleaning_alliase_proximity_ratio",
    "pdm_bins",
    "half_window",
    "poly_deg",
    "limit_to_single_winow",
    "single_window_poly_deg",
]
OUTPUT_FILENAME = "lightcurve_{timestamp:%Y-%m-%d_T%H%M%S}"
DEBUG_FILE_HANDLING = False


@BP.route("/analyse", methods=["POST"])
def analyse():  # pylint: disable=R0914,R0914,R0912,R0915,R0911
    """Performs analysis of an uploaded lightcurve and returns the results in a zip file"""

    if "file" not in request.files:
        response = {"status": "error", "message": "No file uploaded"}
        ERROR_COUNTER.labels(endpoint=request.path, error_type="No file uploaded").inc()
        return jsonify(response), 400

    input_kvn = request.files["file"]
    # kvm_filename = os.path.splitext(kvm.filename)[0]

    if not (input_kvn.filename.endswith(".xml") or input_kvn.filename.endswith(".kvn")
            or input_kvn.filename.endswith(".tdm")):
        response = {
            "status": "error",
            "message": "Invalid file type (expected .xml or .tdm or .kvn extension)",
        }
        ERROR_COUNTER.labels(endpoint=request.path, error_type="Wrong file extension").inc()
        return jsonify(response), 400

    with tempfile.TemporaryDirectory() as tempdir_name:
        # Save input lightcurve in kvn format
        # Note: Input filename is re-used by eon02_opt.LightCurve for the output filename
        input_filename = os.path.join(tempdir_name,
                                      OUTPUT_FILENAME.format(timestamp=datetime.now()) + ".xml")
        input_kvn.save(input_filename)

        # Parse input parameters
        try:
            analysis_params = {
                key: (float(request.form.get(key)) if "." in request.form.get(key) else int(
                    request.form.get(key)))
                for key in EON_OPT02_ANALYSIS_PARAMS_NAMES if key in request.form
            }
        except ValueError as err:
            error_value = str(err).split("'")[1]
            response = {
                "status": "error",
                "message": f"Error during parameter parsing. Invalid value: '{error_value}'",
            }
            ERROR_COUNTER.labels(endpoint=request.path, error_type="Invalid parameter(s)").inc()
            return jsonify(response), 400

        if not DEBUG_FILE_HANDLING:
            # Perform lightcurve analysis
            try:
                lightcurve = LightCurve(input_filename, acc_key=settings.SENSOR_KEY)
            except AttributeError:
                response = {
                    "status": "error",
                    "message": "Error in the file contents. Is SENSORID missing?",
                }
                ERROR_COUNTER.labels(endpoint=request.path,
                                     error_type="Missing attribute in input file").inc()
                return jsonify(response), 400
            lightcurve.analyse(**analysis_params, export=tempdir_name)
        else:
            with open(os.path.join(tempdir_name, "example.txt"), "wt",
                      encoding="utf-8") as fp_example:
                fp_example.write("This is an example file\n")

        # Remove input data to prevent accidentially returning it to the user
        os.remove(input_filename)
        fp_zip_file_path = None
        # Prepare output zip file
        with tempfile.NamedTemporaryFile("w+b", delete=False) as fp_zip_file:
            output_filenames = os.listdir(tempdir_name)

            if len(output_filenames) == 0:
                response = {
                    "status": "error",
                    "message": "Lightcurve analysis module did not create any output files.",
                }
                ERROR_COUNTER.labels(endpoint=request.path,
                                     error_type="No output files created").inc()
                return jsonify(response), 400

            output_filenames = os.listdir(tempdir_name)

            with zipfile.ZipFile(fp_zip_file.name, "w", zipfile.ZIP_DEFLATED) as zip_file:
                for filename in output_filenames:
                    zip_file.write(os.path.join(tempdir_name, filename), arcname=filename)

            fp_zip_file.seek(0)
            zipfile_timestamp = datetime.now().strftime("%Y%m%d%H%M%S%f")
            fp_zip_file_path = fp_zip_file.name

        try:
            upload_lightcurve_to_datacube(zipfile_timestamp, fp_zip_file_path)
        except requests.exceptions.RequestException:
            error_message = "Could not upload analysis results to DataCube"
            ERROR_COUNTER.labels(endpoint=request.path,
                                 error_type="Failure to upload results").inc()
            response = {"status": "error", "message": error_message}
            return jsonify(response), 500

        response = send_file(
            fp_zip_file.name,
            mimetype="application/zip",
            as_attachment=True,
            attachment_filename="output.zip",
        )

        response.call_on_close(lambda: analyse_cleanup(fp_zip_file_path))

        return response


@BP.route("/analyse-multiple", methods=["POST"])
def analyse_multiple():  # pylint: disable=R0914,R0914,R0912,R0915,R0911
    """Performs analysis of multiple uploaded lightcurves and returns the results in a zip file"""

    if not request.files:
        response = {"status": "error", "message": "No file uploaded"}
        ERROR_COUNTER.labels(endpoint=request.path, error_type="No file uploaded").inc()
        return jsonify(response), 400
    with tempfile.TemporaryDirectory() as tempdir_name:
        # Iterate over uploaded files
        for file_key in request.files:
            input_kvn = request.files[file_key]

            if not (input_kvn.filename.endswith(".xml") or input_kvn.filename.endswith(".kvn")
                    or input_kvn.filename.endswith(".tdm")):
                response = {
                    "status": "error",
                    "message": "Invalid file type (expected .xml or .tdm or .kvn extension)",
                }
                ERROR_COUNTER.labels(endpoint=request.path,
                                     error_type="Wrong file extension").inc()
                return jsonify(response), 400

            # If the norad is a valid integer, treat it as norad_id just for directory naming
            try:
                norad, file_no = secure_filename(file_key).split("_")
                subdir_name = os.path.join(tempdir_name, f"noradID_{int(norad)}")
                file_no = int(file_no)
            except ValueError:
                response = {
                    "status":
                    "error",
                    "message": (f"Invalid type for file key '{secure_filename(file_key)}'"
                                f"(must be in the format 'noradID_filenumber', e.g. '12345_1')"),
                }
                ERROR_COUNTER.labels(
                    endpoint=request.path,
                    error_type="File field name not following convention",
                ).inc()
                return jsonify(response), 400

            os.makedirs(subdir_name, exist_ok=True)

            input_filename = os.path.join(tempdir_name, secure_filename(input_kvn.filename))
            input_kvn.save(input_filename)

            try:
                analysis_params = {
                    key: (float(request.form.get(key)) if "." in request.form.get(key) else int(
                        request.form.get(key)))
                    for key in EON_OPT02_ANALYSIS_PARAMS_NAMES if key in request.form
                }
            except ValueError as err:
                error_value = str(err).split("'")[1]
                response = {
                    "status": "error",
                    "message": f"Error during parameter parsing. Invalid value: '{error_value}'",
                }
                ERROR_COUNTER.labels(endpoint=request.path,
                                     error_type="Invalid parameter(s)").inc()
                return jsonify(response), 400

            if not DEBUG_FILE_HANDLING:
                try:
                    lightcurve = LightCurve(input_filename, acc_key=settings.SENSOR_KEY)
                except AttributeError:
                    response = {
                        "status": "error",
                        "message": "Error in the file contents. Is SENSORID missing?",
                    }
                    ERROR_COUNTER.labels(
                        endpoint=request.path,
                        error_type="Missing attribute in input file",
                    ).inc()
                    return jsonify(response), 400
                lightcurve.analyse(**analysis_params, export=subdir_name)

                history_filename = os.path.join(subdir_name, f"history_{norad}.txt")
                total_history_filename = os.path.join(subdir_name, f"history_{norad}_total.txt")
                if not os.path.exists(total_history_filename):
                    try:
                        shutil.copy(history_filename, total_history_filename)
                    except FileNotFoundError:
                        response = {
                            "status":
                            "error",
                            "message":
                            ("NoradID in file key does not correspond to NoradId in file contents"
                             "(must be in the format 'noradID_filenumber', e.g. '12345_1')"),
                        }
                        ERROR_COUNTER.labels(
                            endpoint=request.path,
                            error_type="NoradID in file key does not correspond \
                            to NoradId in file contents",
                        ).inc()
                        return jsonify(response), 400
                else:
                    with open(history_filename, "r", encoding="utf-8") as source_file, open(
                            total_history_filename, "a", encoding="utf-8") as target_file:
                        last_line = source_file.readlines()[-1]
                        target_file.write(last_line)
                os.remove(history_filename)
            else:
                with open(os.path.join(subdir_name, "example.txt"), "wt",
                          encoding="utf-8") as fp_example:
                    fp_example.write("This is an example file\n")

            os.remove(input_filename)

        fp_zip_file_path = None
        # Prepare output zip file
        with tempfile.NamedTemporaryFile("w+b", delete=False) as fp_zip_file:
            with zipfile.ZipFile(fp_zip_file.name, "w", zipfile.ZIP_DEFLATED) as zip_file:
                # Walk the directory structure and add all files to the zip
                for root, _, files in os.walk(tempdir_name):
                    for file in files:
                        full_path = os.path.join(root, file)
                        relative_path = os.path.relpath(full_path, tempdir_name)
                        zip_file.write(full_path, arcname=relative_path)
            fp_zip_file.seek(0)
            zipfile_timestamp = datetime.now().strftime("%Y%m%d%H%M%S%f")
            fp_zip_file_path = fp_zip_file.name

        try:
            upload_lightcurve_to_datacube(zipfile_timestamp, fp_zip_file_path)
        except requests.exceptions.RequestException:
            error_message = "Could not upload analysis results to DataCube"
            ERROR_COUNTER.labels(endpoint=request.path,
                                 error_type="Failure to upload results").inc()
            response = {"status": "error", "message": error_message}
            return jsonify(response), 500

        response = send_file(
            fp_zip_file_path,
            mimetype="application/zip",
            as_attachment=True,
            attachment_filename="output.zip",
        )

        response.call_on_close(lambda: analyse_cleanup(fp_zip_file_path))

        return response


@BP.route("/metrics")
def metrics():
    """Endpoint to provide health metrics"""
    return Response(generate_latest(), mimetype=CONTENT_TYPE_LATEST)
