"""
This module provides methods for reading positional observations
of satellites in the IOD format.

The IOD Observation Format Description is provided at
http://www.satobs.org/position/IODformat.html
"""

import re
from datetime import datetime

import numpy as np

IOD_REGEX = re.compile(
    r"""
    ^(\d{5})\s
    (\d{2})\s
    (.*)\s
    (\d{4})\s
    (\w)\s
    (\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{3})\s
    (\d{2})\s
    (\d{1})(\d{1})\s
    (\d{2})(\d{2})(\d{3})
    ([+-])
    (\d{2})(\d{2})(\d{2})\s
    (\d{2})
    (\s(\w))?""", re.VERBOSE)


class IODParseError(Exception):
    '''
    Raised when an IOD string can't be parsed.
    '''
    def __init__(self, iod_str):
        self.iod_str = iod_str

        self.message = f"This is not a valid IOD string: {iod_str}"
        super(IODParseError, self).__init__(self.message)


def parse_iod(iod_str):
    '''
    ra       Observed RA in radians
    dec      Observed DEC in radians
    norad_id NORAD ID
    site_id  Station ID
    jd       Julian Date
    '''
    iod = iod_str.ljust(80)
    match = re.match(IOD_REGEX, iod)

    if match is None:
        raise IODParseError(iod_str)

    # Extract record from matches
    rec = {
        "norad_id": int(match.group(1)),
        "desig": (match.group(2), match.group(3)),
        "site_id": int(match.group(4)),
        "station_status_code": match.group(5),
        "datetime_raw": list([int(match.group(i)) for i in (6, 7, 8, 9, 10, 11, 12)]),
        "time_uncertainty": match.group(13),
        "angle_format_code": match.group(14),
        "epoch_code": match.group(15),
        "ra_raw": list([int(match.group(i)) for i in (16, 17, 18)]),
        "dec_sign": (1 if match.group(19) == '+' else -1),
        "dec_raw": list([int(match.group(i)) for i in (20, 21, 22)]),
        # "pos_uncertainty": match.group(17),
        # "optical_behavior_code": match.group(18),
        # "visual_mag_sign": match.group(19),
        # "visual_mag": match.group(20),
        # "mag_uncertainty": match.group(21),
        # "flash_period": match.group(22),
    }

    if rec['angle_format_code'] == '1':
        # Format 1: RA/DEC = HHMMSSs+DDMMSS
        rec['ra'] = rec['ra_raw'][0] + \
            rec['ra_raw'][1] / 60 + \
            rec['ra_raw'][2] / 36000

        rec['dec'] = rec['dec_sign'] * \
            rec['dec_raw'][0] + \
            rec['dec_raw'][1] / 60 + \
            rec['dec_raw'][2] / 3600
    elif rec['angle_format_code'] == '2':
        # Format 2: RA/DEC = HHMMmmm+DDMMmm
        rec['ra'] = rec['ra_raw'][0] + \
            rec['ra_raw'][1] / 60 + \
            rec['ra_raw'][2] / 60000

        rec['dec'] = rec['dec_sign'] * \
            rec['dec_raw'][0] + \
            rec['dec_raw'][1] / 60 + \
            rec['dec_raw'][2] / 6000
    else:
        raise NotImplementedError(f'Angle Format {rec["angle_format_code"]} not supported yet')

    rec['ra'] = np.radians(rec['ra'] * 360 / 24)
    rec['dec'] = np.radians(rec['dec'])
    rec['datetime'] = datetime(
        rec['datetime_raw'][0],  # YYYY
        rec['datetime_raw'][1],  # mm
        rec['datetime_raw'][2],  # dd
        rec['datetime_raw'][3],  # HH
        rec['datetime_raw'][4],  # MM
        rec['datetime_raw'][5],  # SS
        rec['datetime_raw'][6] * 1_000)  # .SSS

    return rec
