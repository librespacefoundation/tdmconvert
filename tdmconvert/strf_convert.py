"""
This module provides methods to convert RF measurements given in
the STRF format into CCSDS TDM format.

STRF:
https://github.com/cbassa/strf

CCSDS Tracking Data Messages (CCSDS 503.0-B-2, published June 2020):
https://public.ccsds.org/Pubs/503x0b2c1.pdf
"""
from pathlib import Path

import numpy as np
from beyond.dates import Date
from beyond.io.ccsds import dumps
from beyond.utils.measures import MeasureSet, ReceiveFrequency, TransmitFrequency


def strf2tdm(doppler_filename, output_filename, tdm_format, obs_metadata, originator='N/A'):
    '''Read passive doppler tracking data from STRF datafile and write to a CCSDS TDM file.

    Args:
        doppler_filename (str): filename of the input STRF datafile
        output_filename (str): filename for the output CCSDS TDM datafile
        tdm_format (str): Output format of the file, can be 'xml' or 'kvn'.
        obs_metadata (dict): Metadata of the observation, required keys:
                             satellite_id (str): SatNOGS Satellite ID of the tracked satellite
                             frequency_tx (float): Transmit Frequency of the Satellite Transponder
                                                   in Hz
                             ground_station_id (int): SatNOGS Ground Station ID of the
                                                      receiving station
        originator (str): Creating agency. Value should be an entry from the ‘Abbreviation’
                          column in the SANA Organizations Registry,
                          https://sanaregistry.org/r/organizations/organizations.html
    Return:
        None
    '''
    with open(doppler_filename) as fname:
        doppler_obs = np.loadtxt(fname)

    measures = MeasureSet([])
    measures.append(
        TransmitFrequency(
            path=[str(obs_metadata['ground_station_id']), obs_metadata['satellite_id']],
            participant=2,
            date=Date(doppler_obs[0][0]),  # Use first date of the data (just a filler value :/)
            value=obs_metadata['frequency_tx']))

    for date, frequency_rx, _, _ in doppler_obs:
        measures.append(
            ReceiveFrequency(
                path=[str(obs_metadata['ground_station_id']), obs_metadata['satellite_id']],
                participant=1,
                date=Date(date),
                value=frequency_rx - obs_metadata['frequency_tx']))

    Path(output_filename).write_text(
        dumps(measures, fmt=tdm_format, meta_args={}, originator=originator))
