"""
This module provides methods related to logging
using the Prometheus Python Exporter and the Prometheus Pushgateway.
"""
from prometheus_client import CollectorRegistry, push_to_gateway

import settings

PG_REGISTRY = CollectorRegistry()


def push_metrics(registry=PG_REGISTRY):
    """
    Push the collected metrics of the given registry to the Prometheus pushgateway.
    """
    push_to_gateway(settings.PUSHGATEWAY_URL, job='ingest_seesat', registry=registry)
