"""
Settings module for the ingestion service

This module gets imported in two different way:
- as a normal module via ``import settings``
- as "Celery Config Module" with certain expected variables
"""

from decouple import Csv, config

CELERY_BROKER_URL = config('CELERY_BROKER_URL', default='redis://redis:6379/0')
CELERY_RESULT_BACKEND = config('CELERY_RESULT_BACKEND', default='redis://redis:6379/0')
CELERY_BEAT_SCHEDULE_FILENAME = config('CELERY_BEAT_SCHEDULE_FILENAME',
                                       default='/var/lib/celerybeat/schedule')

IMAP_USER = config('IMAP_USER', default='')
IMAP_PASSWORD = config('IMAP_PASSWORD', default='')
IMAP_HOSTNAME = config('IMAP_HOSTNAME', default='')
SUBMITTER_ALLOWLIST = config('SUBMITTER_ALLOWLIST', cast=Csv(str))
FORWARDING_SERVICE_BASE_URL = config('FORWARDING_SERVICE_BASE_URL', default='')
TDM_ORIGIN = config('TDM_ORIGIN', default='LSF')
PUSHGATEWAY_URL = config('PUSHGATEWAY_URL', default='pushgateway:9091')
