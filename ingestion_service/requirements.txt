celery[redis]
python-decouple
requests
git+https://github.com/kerel-fs/beyond/@tdm_dev#egg=beyond
git+https://gitlab.com/librespacefoundation/tdmconvert.git
prometheus-client
pytz