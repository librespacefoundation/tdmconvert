"""
This module provides methods to communicate with the forwarding service.
"""
import requests


def forward_tdms(forwarding_service_base_url, tdms):
    """
    This method submits the provided CCSDS TDM messages to the forwarding service
    using its HTTP REST API.

    Arguments:
    forwarding_service_hostname (string):
    tdms (list) : List of strings, each containing one CCSDS TDM message (utf-8 encoded).

    Raises:
    requests.exceptions.RequestException
    """
    json_body = {'tdms': tdms}
    response = requests.put(f"{forwarding_service_base_url}/forward_tdms", json=json_body)
    response.raise_for_status()
