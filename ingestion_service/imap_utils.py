"""
This module is a thin wrapper around the built-in imaplib module,
implementing an IMAP client.
"""
# Pylint doesn't allow a proper definition of the exit(_, type, _) method, thus:
# pylint: disable=redefined-builtin

import imaplib
from collections import namedtuple

IMAPConfig = namedtuple('IMAPConfig', 'user password hostname')


class IMAPConnection:
    """
    A thin wrapper around the IMAP4 library
    implementing a context manager
    """
    def __init__(self, imap_config, mailbox='INBOX'):
        self.imap_user = imap_config.user
        self.imap_password = imap_config.password
        self.imap_hostname = imap_config.hostname
        self.mailbox = mailbox
        self.connection = None

    def __enter__(self):
        self.connection = imaplib.IMAP4_SSL(self.imap_hostname)
        self.connection.login(self.imap_user, self.imap_password)
        self.connection.select(self.mailbox)
        return self

    def __exit__(self, type, value, traceback):
        self.connection.close()
        self.connection.logout()

    def search(self, criteria):
        """
        Search the selected mailbox via the SEARCH command

        Returns
        A list of all message numbers/UIDs that satisfy the SEARCH criteria
        """
        _, response = self.connection.search(None, criteria)
        msg_nums = response[0].split()
        return msg_nums

    def mark_seen(self, msg_num):
        """
        Mark the selected messages as "Seen" on the server.

        :param msg_num: List of messages numbers of the selected messages.
        :type msg_num: list[int]
        """
        self.connection.store(msg_num, '+FLAGS', r'\Seen')
