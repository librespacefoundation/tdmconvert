"""
This module provides methods to feth messages from the seesat-l mailing list.
"""

import email
from datetime import timezone
from email import policy

from imap_utils import IMAPConnection


def fetch_seesat(imap_config, submitter_allowlist, filter_unseen=True, peek=True):
    """
    A method to fetch mails from the seesat-l mailinglist.

    Arguments:
    :param imap_config: IMAPConfig(imap_user, imap_password, imap_hostname)
    :param submitter_allowlist:
    :param filter_unseen: Only show unread messages, default: True
    :param peek: Do not alter the read/unread status of the messages, default: True

    :type imap_config: namedtuple
    :type submitter_allowlist: list[str]
    :type filter_unseen: bool
    :type peek: bool
    """
    with IMAPConnection(imap_config) as connection:
        messages = []
        for submitter in submitter_allowlist:
            criterion = f'(HEADER REPLY-TO "{submitter}" {"UNSEEN" if filter_unseen else ""})'

            msg_nums = connection.search(criterion)
            for num in msg_nums:
                if peek:
                    _, data = connection.connection.fetch(num, '(BODY.PEEK[])')
                else:
                    _, data = connection.connection.fetch(num, '(BODY[])')

                email_parsed = email.message_from_bytes(data[0][1], policy=policy.default)
                date = email.utils.parsedate_to_datetime(email_parsed['Date']).astimezone(
                    timezone.utc).replace(tzinfo=None)

                messages.append({
                    'body':
                    email_parsed.get_payload()[0]
                    if email_parsed.is_multipart() else email_parsed.get_payload(),
                    'date':
                    date,
                    'message_id':
                    email_parsed['Message-ID'],
                    'imap_msg_num':
                    num,
                    'reply_to':
                    email_parsed['Reply-To'],
                })

    return messages


def mark_messages_as_seen(imap_config, msg_nums):
    """
    Mark the selected messages as "Seen".

    :param imap_config: IMAPConfig(imap_user, imap_password, imap_hostname)
    :type imap_config: namedtuple
    """
    with IMAPConnection(imap_config) as connection:
        for msg_num in msg_nums:
            connection.mark_seen(msg_num)
