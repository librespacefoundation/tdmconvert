# What's new

## tdmconvert 1.0 - 2023-04-25
- Initial tagged release. Features:
  - TDM Conversion Library "tdmconvert"
  - TDM Forwarding Service, based on `tdmconvert` package
  - Light Curve Analysis Service, based on `eon_opt02` package
- Dependencies versions:
  - [EON Opt 02](https://github.com/alexsiakas/EON_Opt02)@[e964e91](https://github.com/alexsiakas/EON_Opt02/commit/e964e916ced9c3a5fdb87cb0129631f4394c6d05)
