#!/usr/bin/env python3
import argparse
from pathlib import Path

from beyond.io.ccsds import dumps

from tdmconvert import read_iod_lines

DESCRIPTION = """
Convert IOD measurements to CCSDS TDM.

Example usage:
./iod2tdm.py tests/data/2020-09-15_seesat_PP-Obs_Catalog_iod.txt --format xml
"""


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=DESCRIPTION)
    parser.add_argument('IOD_FILE', type=str,
                        help='File with IOD measurements')
    parser.add_argument('--output_folder', type=str,
                        help='Output Folder for the CCSDS TDM files',
                        default='./out/')
    parser.add_argument('--format', type=str,
                        help="Format: kvn or xml, default: kvn",
                        default='kvn')
    parser.add_argument('--originator', type=str,
                        help="Originator",
                        default='LSF')
    args = parser.parse_args()

    Path(args.output_folder).mkdir(exist_ok=True)

    iod_str = Path(args.IOD_FILE).read_text()
    measures_collection = read_iod_lines(iod_str)

    for path, measures in measures_collection.items():
        tdm_str = dumps(measures, fmt=args.format, creation_date=measures[-1].date,
                        originator=args.originator)

        # Safe, as there is only one path per MeasuresSet. Guaranteed by tdmconvert.read_iod_lines
        norad_id, site_id = measures.paths[0]
        output_filename = Path(args.output_folder) / f'{norad_id}-{site_id}.{args.format}'

        Path(output_filename).write_text(tdm_str)
